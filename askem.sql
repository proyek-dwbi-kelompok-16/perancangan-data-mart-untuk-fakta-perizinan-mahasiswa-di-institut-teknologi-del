CREATE TABLE Dim_Date (
	[ID_Date] [int] not null,
	[Year] [timestamp] not null,
	[Month] [int] not null,
	[Month_Name] [varchar] not null,
	[Week_Of_Year] [varchar] not null,
	[Day_Of_Year] [varchar] not null,
	[Date] [date] not null,
	PRIMARY KEY CLUSTERED 
(
	[ID_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) 
GO

CREATE TABLE Dim_JenisIzin (
	[ID_Izin] [varchar] not null,
	[Jenis_Izin] [varchar] not null,
	[Alasan_Izin] [varchar] not null,
	[Version] [int] not null,
	PRIMARY KEY CLUSTERED 
(
	[ID_Izin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) 
GO

CREATE TABLE Dim_Mahasiswa (
	[Nim_Mahasiswa] [varchar] not null,
	[Nama_Mahasiswa] [varchar] not null,
	[Jenis_Kelamin] [varchar] not null,
	[Fakultas] [varchar] not null,
	[Prodi] [varchar] not null,
	[Asrama] [varchar] not null,
	PRIMARY KEY CLUSTERED 
(
	[Nim_Mahasiswa] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) 
GO

CREATE TABLE Dim_Status_Izin (
	[Id_Status_Izin] [int] not null,
	[Status_Izin] [varchar] not null,
	[Approver] [varchar] not null,
	[Version] [varchar] not null,
	[Tanggal_Konfirmasi] [date] not null,
	[Alasan_Konfirmasi] [varchar] not null,
	[Jumlah_Izin] [int] not null,
	PRIMARY KEY CLUSTERED 
(
	[Id_Status_Izin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) 
GO

CREATE TABLE [dbo].[Fact_Table](
	[Nim_Mahasiswa] [varchar] not null,
	[ID_Izin] [varchar] not null,
	[Id_Status_Izin] [int] not null,
	[ID_Date] [int] not null,
	[Asrama] [varchar] not null,
	[Version] [varchar] not null,
	[Jenis_Izin] [varchar] not null,
	[Alasan_Izin] [varchar] not null,
	[Tanggal_Konfirmasi] [date] not null,
	
	PRIMARY KEY CLUSTERED 
(
	[Nim_Mahasiswa] ASC,
	[ID_Izin] ASC,
	[Id_Status_Izin] ASC,
	[ID_Date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Fact_Table]  WITH CHECK ADD  CONSTRAINT [FK_Dim_Date_ID_Date] FOREIGN KEY([ID_Date])
REFERENCES [dbo].[Dim_Date] ([ID_Date])
GO
ALTER TABLE [dbo].[Fact_Table] CHECK CONSTRAINT [FK_Dim_Date_ID_Date]
GO

ALTER TABLE [dbo].[Fact_Table]  WITH CHECK ADD  CONSTRAINT [FK_Dim_JenisIzin_ID_Izin] FOREIGN KEY([ID_Izin])
REFERENCES [dbo].[Dim_JenisIzin] ([ID_Izin])
GO
ALTER TABLE [dbo].[Fact_Table] CHECK CONSTRAINT [FK_Dim_JenisIzin_ID_Izin]
GO

ALTER TABLE [dbo].[Fact_Table]  WITH CHECK ADD  CONSTRAINT [FK_Dim_Mahasiswa_Nim_Mahasiswa] FOREIGN KEY([Nim_Mahasiswa])
REFERENCES [dbo].[Dim_Mahasiswa] ([Nim_Mahasiswa])
GO
ALTER TABLE [dbo].[Fact_Table] CHECK CONSTRAINT [FK_Dim_Mahasiswa_Nim_Mahasiswa]
GO

ALTER TABLE [dbo].[Fact_Table]  WITH CHECK ADD  CONSTRAINT [FK_Dim_Status_Izin_Id_Status_Izin] FOREIGN KEY([Id_Status_Izin])
REFERENCES [dbo].[Dim_Status_Izin] ([Id_Status_Izin])
GO
ALTER TABLE [dbo].[Fact_Table] CHECK CONSTRAINT [FK_Dim_Status_Izin_Id_Status_Izin]
GO