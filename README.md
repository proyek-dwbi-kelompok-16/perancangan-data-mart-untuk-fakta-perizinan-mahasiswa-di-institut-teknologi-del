# PERANCANGAN DATA MART UNTUK FAKTA PERIZINAN MAHASISWA di INSTITUT TEKNOLOGI DEL

1. Latar Belakang

    Institut Teknologi Del merupakan salah satu institutusi pendidikan yang terletak di desa Sitoluama, Kecamatan Laguboti, Kabupaten Toba. Institut Teknologi Del juga merupakan salah satu institusi yang memiliki fasilitas dan teknologi yang tidak kalah bersaing dengan institusi atau universitas lain meskipun institutusi ini terletak pada pedesaan. Adapun beberapa fasilitas teknologi yang dimiliki oleh institut Teknologi Del adalah Campus Information System (CIS), ecourse, zimbra dan sistem barcode yang digunakan sebagai penanda kegiatan keluar-masuk civitas Del. Namun, belum semua data pada institusi ini dapat diintegrasikan dengan baik sehingga sulit untuk dianalisis. Selain itu, masih banyak data yang bersifat redundan sehingga mengakibatkan kurangnya konsistensi baik dalam masalah waktu, pembaharuan data dan kapasitas peyimpanan data. Salah satu solusi yang dapat mendukung terbetuknya sebuah sistem informasi yang terintegrasi dengan baik yaitu dengan menyediakan sebuah basis data yang dapat dipresentasikan dalam berbagai bentuk yang konsisten adalah data mart. Data mart adalah bagian dari data warehouse yang dirancang untuk menganalisis data. Dengan demikian data mart diharapkan akan mampu meningkatkan kekonsistensian sistem berdasarkan analisis subjek yang ada serta mendukung pengambilan keputusan pihak manajemen yang ada pada Institut Teknologi Del.

2. Tujuan

    Tujuan dari proyek Perancangan Data Mart Untuk Fakta Perizinan Mahasiswa yang akan dibangun ini adalah sebagai berikut:

    a.	Mempermudah akses dan meningkatkan efisiensi pada data yang digunakan.
    b.	Meningkatkan keakuratan pada penyimpanan data.
    c.	Meningkatkan waktu response-time yang dibutuhkan oleh user pada saat mengakses data.
    d.	Meningkatkan fleksibilitas dari data.
    e.	Mendukung proses analisis dan decision making untuk setiap kegiatan mahasiswa. 

